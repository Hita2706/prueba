package com.prueba.api.exception;

import java.util.Date;

public class Error {
	
	private Date timestamp;
	private String mensaje;
	private String detalles;

	public Error(Date timestamp, String mensaje, String detalles) {
		super();
		this.timestamp = timestamp;
		this.mensaje = mensaje;
		this.detalles = detalles;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return mensaje;
	}

	public String getDetails() {
		return detalles;
	}
}
